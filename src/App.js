import React, { useState } from 'react'
import { createGame, guessAnswer, getAnswer } from './api'

const validateGuessItem = input => !input || /^[0-9]$/.test(input)
const validateGuess = guess => /^[0-9]{4}$/.test(guess.join(''))

export const App = () => {
  const [guess, setGuess] = useState(Array(4).fill('')) // ['', '', '', '']
  const [guesses, setGuesses] = useState([])
  const [game, setGame] = useState() // '/api/games/1'
  const [answer, setAnswer] = useState()
  const [correct, setCorrect] = useState(false)

  const handleNewGame = () => {
    createGame()
      .then(url => {
        setGame(url)
        setGuess(Array(4).fill(''))
        setGuesses([])
        setCorrect(false)

        // ! tricky
        getAnswer(url).then(setAnswer)
      })
  }

  const handleGuess = () => {
    guessAnswer(game, { answer: guess.join('') })
      .then(result => {
        setGuesses([{ ...result, guess }].concat(guesses))
        setCorrect(result.correct)
      })
  }

  return (
    <main>
      <table style={{ textAlign: 'center' }}>
        <tbody>
          <tr>
            <td colSpan='4' style={{ textAlign: 'left' }}>
              <button onClick={handleNewGame}>New Game</button>
              <span style={{ color: '#eee' }}>{ answer && answer.answer }</span>
            </td>
            <td></td>
          </tr>
          {
            !game ? null :
              <>
                <tr>
                  {
                    guess.map((value, index) => (
                      <td key={index}>
                        <input
                          style={{ textAlign: 'center' }}
                          value={value}
                          type='number'
                          onChange={event => {
                            const input = event.target.value
                            if (validateGuessItem(input)) {
                              const newGuess = [...guess]
                              newGuess[index] = input
                              setGuess(newGuess)
                            }
                          }}
                        />
                      </td>
                    ))
                  }
                  <td></td>
                </tr>
                <tr>
                  <td colSpan='4'>
                    {
                      correct ?
                        <button>😂 correct</button> :
                        <button
                          disabled={!validateGuess(guess)}
                          onClick={handleGuess}
                        >Guess</button>
                    }
                  </td>
                </tr>
                {
                  guesses.map(({ guess, hint, correct }, index) => (
                    <tr key={index}>
                      {guess.map((value, index) => (
                        <td key={index}>{value}</td>
                      ))}
                      <td>
                        <span style={{ color: correct ? 'green' : 'red' }}>{hint}</span>
                      </td>
                    </tr>
                  ))
                }
              </>
          }
        </tbody>
      </table>
    </main>
  )
}
